var priceInput = document.getElementById('priceInput');
var percentBoxes = document.getElementsByClassName('PercentBox__Value');
var percentBoxesLabel = document.getElementsByClassName('PercentBox__Label');
// var priceListCache = [1,2,3,4,5,6];
var priceListCache = [];

function getPrice(percentage) {
  var result = (priceInput.value * percentage).toString();
  var trimmedResult = result.substring(0, 8);
  if (trimmedResult.charAt(trimmedResult.length - 1) == '.') {
  	return result.substring(0, 9);
  }
  return trimmedResult;
}

function init(){
	chrome.storage.sync.get("priceList", (data) => {
	    console.log('Read the priceList ', data);	
	    for (var i = 0; i < percentBoxes.length; i++) {
	    	var prefix = data.priceList[i] > 0 ? '+' : '';
	    	percentBoxesLabel[i].innerText = prefix + data.priceList[i] + '%';
		}
		priceListCache = data.priceList;
	});

	priceInput.addEventListener('input', function(e) {
	    if(isNaN(priceInput.value)) return; 

	    for (var i = 0; i < percentBoxes.length; i++) {
	    	var multiplier = 1 + priceListCache[i]/100;
    		percentBoxes[i].value = getPrice(multiplier);
		}
	}, false);

	setTimeout(() => {  
		document.execCommand('paste');
	}, 300);
}

init();
