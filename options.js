var button = document.getElementsByClassName("OptionsButton")[0];
var percentBoxes = document.getElementsByClassName('PercentBox__Value');

function handleButtonClick(event) {
	var priceList = [];
	for (var i = 0; i < percentBoxes.length; i++) {
		priceList.push(Number(percentBoxes[i].value));
	}
  	chrome.storage.sync.set({ priceList }, function() {
        console.log('Set the priceList to ', priceList);
    });
}

function constructOptions() {
  button.addEventListener("click", handleButtonClick);
  chrome.storage.sync.get("priceList", (data) => {
	    console.log('Read the priceList ', data);
	    for (var i = 0; i < percentBoxes.length; i++) {
	    	percentBoxes[i].value = data.priceList[i];
		}
  });
}

constructOptions();