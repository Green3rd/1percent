### Overview ###

* This extension is to calculate the percentage of the given value.
* Version 1.0
![UI](./ui.jpg)


### How to use ###

* Open the extension and type the value you want. 
* The input field is auto-focus when the extension opens.
* The value from the clipboard is automatically filled if you have one.

### Configurations ###

* The default value of each percentage are 3%, 1%, 0.3% and their minus counterpart.
* You can change these value in the configurations page.
![Configurations Page](./configs.jpg)